import '../styles/index.scss';
import * as moment from 'moment';



// Task 3
window.onload = function() {
    tick();
    setInterval(tick, 100);
  };

function tick() {
    document.getElementById("clock").innerHTML = moment().format('MMMM Do YYYY, h:mm:ss a');;
}


// Task 5
function rockPaperScissor(choice) { 

    // 1 --> Rock    2 --> Paper   3 --> Scissor
    let r = Math.floor((Math.random()* 3) + 1);
    var result = document.getElementById("rps_result"); 


    if(r === 1)                                 
    {
        document.getElementById("computer_play").innerHTML  = "Computer played rock";

        if(choice === "Rock")
        {
            result.innerHTML = "Draw";
        }
        else if(choice === "Paper")
        {
            result.innerHTML = "You win!";
        }
        else
        {
            result.innerHTML = "You loose!";
        }
    }
    else if(r === 2)                             
    {
        document.getElementById("computer_play").innerHTML  = "Computer played paper";

        if(choice === "Rock")
        {
            result.innerHTML = "You loose!";
        }
        else if(choice === "Paper")
        {
            result.innerHTML = "Draw";
        }
        else
        {
            result.innerHTML = "You win!";
        }
    }
    else                                         
    {
        document.getElementById("computer_play").innerHTML  = "Computer played scissor";

        if(choice === "Rock")
        {
            result.innerHTML = "You win!";
        }
        else if(choice === "Paper")
        {
            result.innerHTML = "You lose!";
        }
        else
        {
            result.innerHTML = "Draw";
        }
    }
    
} 


global.rockPaperScissor = rockPaperScissor;

